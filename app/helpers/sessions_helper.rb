module SessionsHelper

  def current_user
    if user_id = cookies[:user_id]
      @current_user ||= User.find_by(id: user_id)
    end
  end

  def signed_in?
    !current_user.nil?
  end

  def sign_in(user)
    cookies.permanent[:user_id] = user.id
  end

  def sign_out
    @current_user = nil
    cookies.delete(:user_id)
  end

end
