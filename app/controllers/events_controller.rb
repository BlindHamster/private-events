class EventsController < ApplicationController

  def index
    @prevoius_events = Event.prevoius
    @upcoming_events = Event.upcoming
  end

  def new
    @event = current_user.events.build
  end

  def show
    @event = Event.find(params[:id])
    @users = @event.guests
  end

  def create
    @event = current_user.events.build(event_params)
    if @event.save
      flash[:success] = "New event created!"
      redirect_to @event
    else
      render 'new'
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to root_url
  end

  private

    def event_params
      params.require(:event).permit(:location, :date)
    end
end
