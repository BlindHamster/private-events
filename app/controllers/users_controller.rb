class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "User created!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
    @upcoming_events = @user.events.upcoming
    @upcoming_attended_events = @user.attended_events.upcoming
    @previous_events = @user.events.previous
    @previous_attended_events = @user.attended_events.previous
  end

  def index
    @users = User.all
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password)
    end
end
