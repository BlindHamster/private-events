class Event < ApplicationRecord
  belongs_to :host, class_name: "User", foreign_key: "host_id"
  has_many :invitations, foreign_key: "event_id", dependent: :destroy
  has_many :guests, through: :invitations, source: :guest

  scope :previous, ->{ where("date < ?", Time.zone.now) }
  scope :upcoming, ->{ where("date >= ?", Time.zone.now) }
end
