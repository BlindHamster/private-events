class User < ApplicationRecord
  has_many :events, foreign_key: 'host_id', dependent: :destroy
  has_many :invitations, foreign_key: "guest_id", dependent: :destroy
  has_many :attended_events, through: :invitations, source: :attended_event

  #Authentification method
  def authenticate(password)
    password == self.password
  end
end
