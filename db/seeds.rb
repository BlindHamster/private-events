# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: "greg", email: "greg@mail.com", password: "password")
puts "User created!"

50.times do |i|
  name = "user_#{i+1}"
  email = "#{name}@mail.com"
  password = "password"
  User.create(name: name, email: email, password: password)
  puts "Users created: #{i+1}/50"
end

users = User.take(5)
count = 0
users.each_with_index do |user, index|
  30.times do |i|
    location = Faker::Address.city + ", " + Faker::Address.street_address
    date = Faker::Date.between(5.days.ago, Date.today)
    description = Faker::Lovecraft.paragraphs.join(" ")
    user.events.create(location: location, date: date, description: description)
    count+= 1
    puts "Messages created: #{count}/150"
  end
end

count = 0
events = Event.all
10.times do |i|
  events.each do |event|
    guest_id = User.find(i+6).id
    event.invitations.create(guest_id: guest_id)
    count+= 1
    puts "#{count}/1500 invitations created"
  end
end
