class AddInvitationsTable < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations, id: false do |t|
      t.integer :event_id, index: true
      t.integer :guest_id, index: true
      t.timestamps
    end
    add_foreign_key :invitations, :events, column: "event_id"
    add_foreign_key :invitations, :users, column: "guest_id"
  end
end
