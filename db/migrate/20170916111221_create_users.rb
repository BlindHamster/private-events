class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :email, index: true
      t.string :name, index: true

      t.timestamps
    end
  end
end
