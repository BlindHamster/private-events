Rails.application.routes.draw do
root 'users#index'
get '/sign_in', to: 'sessions#new'
post '/sign_in', to: 'sessions#create'
delete '/sign_out', to: 'sessions#destroy'
resources :users, only: [:new, :create, :show, :index]
resources :events, only: [:index, :new, :create, :show, :destroy]
end
